python-muranoclient (2.8.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 04 Apr 2024 08:30:45 +0200

python-muranoclient (2.8.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 01 Mar 2024 21:37:20 +0100

python-muranoclient (2.7.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 15:49:43 +0200

python-muranoclient (2.7.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 12 Sep 2023 14:38:40 +0200

python-muranoclient (2.6.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1047674).

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Aug 2023 08:49:04 +0200

python-muranoclient (2.6.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 13:16:05 +0200

python-muranoclient (2.6.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 24 Feb 2023 12:01:49 +0100

python-muranoclient (2.5.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 14:36:31 +0200

python-muranoclient (2.5.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 02 Sep 2022 10:00:25 +0200

python-muranoclient (2.4.1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 11:19:20 +0100

python-muranoclient (2.4.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Feb 2022 09:32:08 +0100

python-muranoclient (2.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 17:48:34 +0200

python-muranoclient (2.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Sep 2021 16:21:02 +0200

python-muranoclient (2.2.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 19:04:05 +0200

python-muranoclient (2.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 09 Mar 2021 10:06:07 +0100

python-muranoclient (2.1.1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2020 16:31:29 +0200

python-muranoclient (2.1.1-1) experimental; urgency=medium

  * Fixed watch file URL.
  * New upstream release.
  * Removed python3-mock from build-depends.
  * Switched to debhelper-compat 11.

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Sep 2020 12:35:53 +0200

python-muranoclient (2.0.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 23:22:24 +0200

python-muranoclient (2.0.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 08 Apr 2020 17:32:48 +0200

python-muranoclient (1.3.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 01:38:59 +0200

python-muranoclient (1.3.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2019 00:11:49 +0200

python-muranoclient (1.2.0-4) unstable; urgency=medium

  * Fixed python3-muranoclient is empty.

 -- Thomas Goirand <zigo@debian.org>  Thu, 18 Jul 2019 14:13:47 +0200

python-muranoclient (1.2.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 13:47:23 +0200

python-muranoclient (1.2.0-2) experimental; urgency=medium

  * Removed debian/python3-muranoclient.postinst (Closes: #925426).

 -- Thomas Goirand <zigo@debian.org>  Mon, 25 Mar 2019 15:12:10 +0100

python-muranoclient (1.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.
  * Add Breaks+Replaces: python-muranoclient (because of man page).

 -- Thomas Goirand <zigo@debian.org>  Sat, 23 Mar 2019 12:28:31 +0100

python-muranoclient (1.1.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Sep 2018 00:15:20 +0200

python-muranoclient (1.1.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Building doc with Python 3.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Aug 2018 11:30:35 +0200

python-muranoclient (1.0.1-4) unstable; urgency=medium

  * Fixed Python 3 shebang.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Mar 2018 18:29:09 +0000

python-muranoclient (1.0.1-3) unstable; urgency=medium

  * Python 3 has now priority over Python 2.

 -- Thomas Goirand <zigo@debian.org>  Sun, 11 Mar 2018 13:12:18 +0100

python-muranoclient (1.0.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 21:54:05 +0000

python-muranoclient (1.0.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Tue, 13 Feb 2018 15:05:22 +0000

python-muranoclient (0.14.0-2) unstable; urgency=medium

  * Uploading to unstable:
    - Fix FTBFS with Python 3.6 (Closes: #867628).

 -- Thomas Goirand <zigo@debian.org>  Sun, 29 Oct 2017 17:42:48 +0100

python-muranoclient (0.14.0-1) experimental; urgency=medium

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.0.1.
  * Deprecating priority extra as per policy 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using pkgos-dh_auto_{install,test}.
  * Removed all patches, applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Tue, 03 Oct 2017 22:28:22 +0200

python-muranoclient (0.8.3-4) unstable; urgency=high

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Removed UPSTREAM_GIT, changed to default value
  * d/copyright: Changed source URL to new one

  [ Thomas Goirand ]
  * CVE-2016-4972: RCE vulnerability in Openstack Murano using insecure YAML
    tags. Adds upstream patch: Use yaml.SafeLoader instead of yaml.Loader.
    (Closes: #828063).
  * Standards-Version is now 3.9.8 (no change).

 -- Thomas Goirand <zigo@debian.org>  Mon, 27 Jun 2016 18:53:01 +0000

python-muranoclient (0.8.3-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 16:34:28 +0000

python-muranoclient (0.8.3-2) experimental; urgency=medium

  * Added Python 3 support.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Mar 2016 09:21:55 +0100

python-muranoclient (0.8.3-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Corey Bryant ]
  * New upstream release.
  * Align (Build-Depends) with upstream.

  [ Thomas Goirand ]
  * New upstream release.
  * Align (Build-Depends) with upstream.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Mar 2016 09:15:05 +0100

python-muranoclient (0.8.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Remove Add_keystone_v3_support_to_client.patch applied upstream.
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Mon, 18 Jan 2016 10:33:34 +0000

python-muranoclient (0.7.1-3) unstable; urgency=medium

  * Add upstream patch for Keystone API v3 support.

 -- Thomas Goirand <zigo@debian.org>  Fri, 06 Nov 2015 13:37:53 +0000

python-muranoclient (0.7.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 22:09:58 +0000

python-muranoclient (0.7.1-1) experimental; urgency=medium

  * New upstream release.
  * d/control: Align dependencies with upstream.

 -- Corey Bryant <corey.bryant@canonical.com>  Mon, 05 Oct 2015 15:46:14 -0400

python-muranoclient (0.7.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 11 Sep 2015 14:09:53 +0200

python-muranoclient (0.5.9-2) unstable; urgency=medium

  * Disable unit tests failing with mock >= 1.3 (Closes: #796463).

 -- Thomas Goirand <zigo@debian.org>  Tue, 25 Aug 2015 18:10:28 +0000

python-muranoclient (0.5.9-1) unstable; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 May 2015 07:49:19 +0000

python-muranoclient (0.5.7-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Apr 2015 20:29:43 +0000

python-muranoclient (0.5.5-1) experimental; urgency=low

  * Initial release (Closes: #762787).

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Sep 2014 15:50:43 +0800
